include_guard(GLOBAL)

if(CONAN_EXPORTED) # in conan local cache
  return()
endif()

if(NOT EXISTS "${CMAKE_CURRENT_BINARY_DIR}/conan.cmake")
  message(STATUS "Downloading conan.cmake from https://github.com/conan-io/cmake-conan")
  file(DOWNLOAD "https://github.com/conan-io/cmake-conan/raw/v0.16.1/conan.cmake"
    "${CMAKE_CURRENT_BINARY_DIR}/conan.cmake"
    EXPECTED_HASH SHA1=18536da9401cfb2539139ea308e2497795ccbc09)
endif()

include(${CMAKE_CURRENT_BINARY_DIR}/conan.cmake)

conan_config_install(ITEM https://gitlab.com/ptr-project/conan-config/-/archive/main/conan-config-main.zip SOURCE conan-config-main/)

if (BUILD_TESTING)
  set(build_testing_option "build_testing=True")
else()
  set(build_testing_option "build_testing=False")
endif()

set(build_preference "outdated")

set(lockfile ${CMAKE_CURRENT_SOURCE_DIR}/conan.lock)
if(EXISTS "${lockfile}")

  set(lock_create_command ${CONAN_CMD} lock create
    ${CMAKE_CURRENT_SOURCE_DIR}/conanfile.py
    --lockfile ${lockfile}
    --lockfile-out conan.lock.full
    --build ${build_preference}
    --profile ptr-default.jinja
    -o ${build_testing_option}
    )

  message(STATUS "Full lock file command: ${lock_create_command}")

  execute_process(COMMAND ${lock_create_command} RESULT_VARIABLE return_code
    WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})

  if(NOT "${return_code}" STREQUAL "0")
    message(FATAL_ERROR "Conan lockfile creation failed='${return_code}'")
  endif()

  conan_cmake_install(PATH_OR_REFERENCE
    ${CMAKE_CURRENT_SOURCE_DIR}/conanfile.py
    LOCKFILE conan.lock.full
    BUILD ${build_preference})

  set_property(DIRECTORY APPEND PROPERTY CMAKE_CONFIGURE_DEPENDS ./conan.lock)

else() # Without lockfile

  conan_cmake_install(PATH_OR_REFERENCE
    ${CMAKE_CURRENT_SOURCE_DIR}/conanfile.py
    PROFILE ptr-default.jinja
    OPTIONS ${build_testing_option}
    BUILD ${build_preference})

endif()

